﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("csv.txt");
            DataConsolePrinter printer = new DataConsolePrinter();

            User user1 = User.GenerateUser("User1");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            printer.Print(proxy1);
            Console.WriteLine();

            User user2 = User.GenerateUser("User2");
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            printer.Print(proxy2);
            Console.WriteLine();

            User user3 = User.GenerateUser("User3");
            ProtectionProxyDataset proxy3 = new ProtectionProxyDataset(user3);
            printer.Print(proxy3);
            Console.WriteLine();

            VirtualProxyDataset proxy4 = new VirtualProxyDataset("csv.txt");
            printer.Print(proxy4);
            Console.WriteLine();
        }
    }
}
