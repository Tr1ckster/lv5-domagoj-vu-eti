﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void Print(IDataset database)
        {
            IReadOnlyCollection<List<string>> Database = database.GetData();

            if (Database == null)
            {
                Console.WriteLine("Access denied");
                return;
            }

            foreach (List<string> lines in Database)
            {
                foreach (string line in lines)
                {
                    Console.Write(line + ' ');
                }
                Console.WriteLine();
            }
        }
    }
}
