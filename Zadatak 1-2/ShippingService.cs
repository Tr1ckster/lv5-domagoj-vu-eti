﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1_2
{
    class ShippingService
    {
        private double PricePerMass;

        public ShippingService(double PricePerMass) { this.PricePerMass = PricePerMass; }

        public double CalculateShippingPrice(double weight)
        {
            return (weight * PricePerMass);
        }
    }
}
